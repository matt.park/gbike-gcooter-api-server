package io.gbike.gbikegcooterapiserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GbikeGcooterApiServerApplication

fun main(args: Array<String>) {
	runApplication<GbikeGcooterApiServerApplication>(*args)
}
