package io.gbike.gbikebatchserver.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class HelloKotlinController {

    @GetMapping()
    fun index(): ResponseEntity<String> {
        val hello = "Hello Kotlin"
        return ResponseEntity.ok(hello)
    }
}