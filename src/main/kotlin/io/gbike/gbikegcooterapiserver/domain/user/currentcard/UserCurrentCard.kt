package io.gbike.gbikegcooterapiserver.domain.user.currentcard

import io.gbike.gbikegcooterapiserver.domain.user.User
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class UserCurrentCard(
    @ManyToOne
    val user: User,

    val cardInfoUserId: Long,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val user_id: Long? = null
) {}