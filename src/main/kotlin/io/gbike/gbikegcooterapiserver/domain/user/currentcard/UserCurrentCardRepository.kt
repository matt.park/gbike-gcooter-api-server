package io.gbike.gbikegcooterapiserver.domain.user.currentcard

import org.springframework.data.jpa.repository.JpaRepository

interface UserCurrentCardRepository : JpaRepository<UserCurrentCard, Long> {

    fun findByUserId(id: Long): UserCurrentCard
}