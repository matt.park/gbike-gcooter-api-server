package io.gbike.gbikegcooterapiserver.domain.user

import io.gbike.gbikegcooterapiserver.domain.user.currentcard.UserCurrentCard
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
class User(

    var nickName: String,

    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], orphanRemoval = true)
    val userCurrentCard: MutableList<UserCurrentCard> = mutableListOf(),


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val user_id: Long? = null
) {

    init {
        if (nickName.isBlank()) {
            throw java.lang.IllegalArgumentException("연락처는 필수 입니다")
        }
    }
}