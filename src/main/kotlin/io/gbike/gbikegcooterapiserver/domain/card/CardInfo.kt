package io.gbike.gbikegcooterapiserver.domain.card

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class CardInfo(

    val card_info: String,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

) {
    init {
        if (card_info.isBlank()) {
            throw IllegalArgumentException("빌키 는 비어 있을 수 없습니다")
        }
    }
}