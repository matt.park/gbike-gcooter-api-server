package io.gbike.gbikegcooterapiserver.domain.card

import org.springframework.data.jpa.repository.JpaRepository

interface CardInfoRepository : JpaRepository<CardInfo, Long> {

    fun findByUserId(id: Long): CardInfo?
}